﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace HaberSizsiniz.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Haber Sizsiniz Bir Sivil Toplum Kuruluşudur";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Bizimle İletişime Geçin";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
